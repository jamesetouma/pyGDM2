Processing
************************************


linear
=========================

**linear** contains evaluation functions for linear effects.

near-field
-----------------------------------------------------------

.. autofunction:: pyGDM2.linear.nearfield

.. autofunction:: pyGDM2.linear.optical_chirality

.. autofunction:: pyGDM2.linear.internal_field_intensity

.. autofunction:: pyGDM2.linear.poynting

.. autofunction:: pyGDM2.linear.field_gradient

.. autofunction:: pyGDM2.linear.optical_force

.. autofunction:: pyGDM2.linear.heat

.. autofunction:: pyGDM2.linear.temperature


far-field
-----------------------------------------------------------

.. autofunction:: pyGDM2.linear.extinct

.. autofunction:: pyGDM2.linear.farfield



multipole decomposition
=========================

.. autofunction:: pyGDM2.multipole.multipole_decomposition_exact

.. autofunction:: pyGDM2.multipole.extinct

.. autofunction:: pyGDM2.multipole.scs



nonlinear
=========================

**nonlinear** contains evaluation functions for non-linear effects.

TPL and SP-LDOS
-------------------------------------

.. autofunction:: pyGDM2.nonlinear.tpl_ldos




fast electrons
=========================

**electron** contains evaluation functions for simulations using fast electron illumination.


electron beam incident field
-------------------------------------

.. autofunction:: pyGDM2.fields.fast_electron


EELS and cathodoluminescence (CL)
-------------------------------------

.. autofunction:: pyGDM2.electron.EELS

.. autofunction:: pyGDM2.electron.CL


tools
-------------------------------------

.. autofunction:: pyGDM2.electron.electron_speed

.. autofunction:: pyGDM2.electron.visu_structure_electron

